/**
 ******************************************************************************
 * @file    RTOS.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de dez de 2017
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_RTOS_HPP
#define SOURCE_PRODUCTION_RTOS_HPP

#include <string>

#include <Production/FreeRTOS/Inc/cmsis_os.h>

constexpr auto semtstSTACK_SIZE = configMINIMAL_STACK_SIZE * 10;

#define SET_THREAD_NAME(name) #name

namespace Rtos
{
    typedef osThreadDef_t       RTOS_ThreadDef;
    typedef osThreadId          RTOS_ThreadID;
    typedef QueueHandle_t       RTOS_QueueHandle;
    typedef os_pthread          RTOS_Thread;
    typedef osPriority          RTOS_Priority;

    class RTOS
    {
       private:

       protected:

       public:
          /**
           * @brief
           */
          RTOS();

          /**
           * @brief
           */
          virtual ~RTOS();

          /**
           * @brief
           * @param _name
           * @param _thread
           * @param _priority
           * @param _instances
           * @param _stackSize
           * @param _argument
           */
          static auto createThread(std::string _threadName,
                                   RTOS_Thread _thread,
                                   RTOS_Priority _priority,
                                   uint32_t _instances,
                                   uint32_t _stackSize,
                                   void* _argument) -> RTOS_ThreadID;

          /**
           * @brief
           * @param _queueSize
           * @param _itemSize
           */
          static auto createQueue(uint32_t _queueSize,
                                  uint32_t _itemSize) -> RTOS_QueueHandle;

          /**
           * @brief
           * @param _queue
           */
          static auto getNumberOfMessagesWaitingFromISR(RTOS_QueueHandle _queue) -> uint32_t;

          /**
           * @brief
           * @param _queue
           * @param _buffer
           * @param _higherPriorityTaskWoken
           */
          static auto receiveMessageFromISR(RTOS_QueueHandle _queue,
                                            void* const _buffer,
                                            BaseType_t* const _higherPriorityTaskWoken) -> uint32_t;

          /**
           * @brief
           * @param _queue
           * @param _buffer
           * @param _tickerToWait
           */
          static auto sendToBack(RTOS_QueueHandle _queue,
                                 void* const _buffer,
                                 uint32_t _tickerToWait) -> int32_t;
    };
}

#endif // SOURCE_PRODUCTION_RTOS_HPP
