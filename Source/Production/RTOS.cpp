/**
 ******************************************************************************
 * @file    RTOS.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de dez de 2017
 * @brief
 ******************************************************************************
 */

#include <Production/RTOS.hpp>

namespace Rtos
{
    RTOS::RTOS()
    {
       // TODO Auto-generated constructor stub
    }

    RTOS::~RTOS()
    {
       // TODO Auto-generated destructor stub
    }

    auto RTOS::createThread(std::string _threadName,
                            RTOS_Thread _thread,
                            RTOS_Priority _priority,
                            uint32_t _instances,
                            uint32_t _stackSize,
                            void* _argument) -> RTOS_ThreadID
    {
       const RTOS_ThreadDef rtos_ThreadDef = { const_cast<char*>(_threadName.c_str()),
                                               _thread,
                                               _priority,
                                               _instances,
                                               _stackSize };

       return osThreadCreate(&rtos_ThreadDef,
                             _argument);
    }

    auto RTOS::createQueue(uint32_t _queueSize,
                           uint32_t _itemSize) -> RTOS_QueueHandle
    {
       return xQueueCreate(_queueSize,
                           _itemSize);
    }

    auto RTOS::getNumberOfMessagesWaitingFromISR(RTOS_QueueHandle _queue) -> uint32_t
    {
       return uxQueueMessagesWaitingFromISR(_queue);
    }

    auto RTOS::receiveMessageFromISR(RTOS_QueueHandle _queue,
                                     void* const _buffer,
                                     BaseType_t* const _higherPriorityTaskWoken) -> uint32_t
    {
       return xQueueReceiveFromISR(_queue,
                                   _buffer,
                                   _higherPriorityTaskWoken);
    }

    auto RTOS::sendToBack(RTOS_QueueHandle _queue,
                          void* const _buffer,
                          uint32_t _tickerToWait) -> int32_t
    {
       return xQueueSendToBack(_queue,
                               _buffer,
                               _tickerToWait);
    }
}
