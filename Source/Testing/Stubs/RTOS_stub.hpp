/**
 ******************************************************************************
 * @file    RTOS_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de dez de 2017
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_RTOS_STUB_HPP
#define SOURCE_TESTING_STUBS_RTOS_STUB_HPP

#include <Production/RTOS.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Rtos
{
    class RTOS_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_RTOS
   {
      public:
         virtual ~I_RTOS();
   };

   class _Mock_RTOS : public I_RTOS
   {
      public:
         _Mock_RTOS();
   };

   typedef ::testing::NiceMock<_Mock_RTOS> Mock_RTOS;

   void stub_setImpl(I_RTOS* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_RTOS_STUB_HPP
