/**
 ******************************************************************************
 * @file    RTOS_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/RTOS_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace Rtos
{
   static I_RTOS* p_RTOS_Impl = NULL;

   const char* RTOS_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle RTOS functions";
   }

   void stub_setImpl(I_RTOS* pNewImpl)
   {
      p_RTOS_Impl = pNewImpl;
   }

   static I_RTOS* RTOS_Stub_Get_Impl(void)
   {
      if(p_RTOS_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to RTOS functions. Did you forget"
                   << "to call Rtos::stub_setImpl() ?" << std::endl;

         throw RTOS_StubImplNotSetException();
      }

      return p_RTOS_Impl;
   }

      ///////////////////////////////
      // I_RTOS Methods Definition //
      ///////////////////////////////

   I_RTOS::~I_RTOS()
   {
      if(p_RTOS_Impl == this)
      {
         p_RTOS_Impl = NULL;
      }
   }

   ///////////////////////////////////
   // _Mock_RTOS Methods Definition //
   ///////////////////////////////////

   _Mock_RTOS::_Mock_RTOS()
   {
   }

               //////////////////////////////////
               // Definition of Non Stub Class //
               //////////////////////////////////

   RTOS::RTOS()
   {
   }

   RTOS::~RTOS()
   {
   }
}
