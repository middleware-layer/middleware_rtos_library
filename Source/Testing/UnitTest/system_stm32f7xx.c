/**
 ******************************************************************************
 * @file    system_stm32f7xx.c
 * @author  MCD Application Team
 * @version V1.2.0
 * @date    30-December-2016
 * @brief   CMSIS Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *   This file provides two functions and one global variable to be called from
 *   user application:
 *      - SystemInit(): This function is called at startup just after reset and
 *                      before branch to main program. This call is made inside
 *                      the "startup_stm32f7xx.s" file.
 *
 *      - SystemCoreClock variable: Contains the core clock (HCLK), it can be used
 *                                  by the user application to setup the SysTick
 *                                  timer or configure other parameters.
 *
 *      - SystemCoreClockUpdate(): Updates the variable SystemCoreClock and must
 *                                 be called whenever the core clock is changed
 *                                 during program execution.
 *
 *
 ******************************************************************************
 */

#include <Production/CMSIS/Device/ST/STM32F7xx/Include/stm32f7xx.h>

#if !defined  (HSE_VALUE)
#define HSE_VALUE    ((uint32_t)25000000) /*!< Default value of the External oscillator in Hz */
#endif /* HSE_VALUE */

#if !defined  (HSI_VALUE)
#define HSI_VALUE    ((uint32_t)16000000) /*!< Value of the Internal oscillator in Hz*/
#endif /* HSI_VALUE */

/************************* Miscellaneous Configuration ************************/

/*!< Uncomment the following line if you need to relocate your vector Table in
 Internal SRAM. */
// #define VECT_TAB_SRAM
#if defined(RELEASE)
#define VECT_TAB_OFFSET  0x000C0000U //! Vector Table base offset field. This value must be a multiple of 0x200
#elif defined(DEBUG)
#define VECT_TAB_OFFSET  0x00000000U //! Vector Table base offset field. This value must be a multiple of 0x200
#endif

#if defined(DATA_IN_ExtSDRAM)
/* Modify the attribute of the default SDRAM region (0xC0000000 - 0xC0800000) to be executable */
#define MPU_SDRAM_EXEC_REGION_NUMBER  1
#define MPU_SDRAM_REGION_TEX          (0x4 << MPU_RASR_TEX_Pos) /* Cached memory */
#define MPU_SDARM_BASE_ADDRESS        0xC0000000
#define MPU_SDRAM_EXEC_REGION_SIZE    (22 << MPU_RASR_SIZE_Pos)  /* 2^(22+1) = 8Mo */
#define MPU_SDRAM_ACCESS_PERMSSION    (0x03UL << MPU_RASR_AP_Pos)

#if !defined (SDRAM_MEM_BUS_WIDTH)
#define SDRAM_MEM_BUS_WIDTH         FMC_SDRAM_MEM_BUS_WIDTH_32
#endif /* !defined (SDRAM_MEM_BUS_WIDTH) */
#endif

/******************************************************************************/

uint32_t SystemCoreClock = 16000000;
const uint8_t AHBPrescTable[16] = { 0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    1,
                                    2,
                                    3,
                                    4,
                                    6,
                                    7,
                                    8,
                                    9 };
const uint8_t APBPrescTable[8] = { 0,
                                   0,
                                   0,
                                   0,
                                   1,
                                   2,
                                   3,
                                   4 };

void SystemInit(void)
{

}

void SystemCoreClockUpdate(void)
{
}

void SetupRunTimeStatsTimer(void)
{
}
