#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_RTOS_Library                                              #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

if(DEFINED STM32F7_DRIVER_LAYER_PROJ_NAME)
elseif((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_DEBUG_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_RELEASE_BUILD_NAME}))
    set(STM32F7_DRIVER_LAYER_PROJ_NAME                  "STM32F7_Driver_Layer")
elseif((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_TESTS_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_STUBS_BUILD_NAME}))
    set(STM32F7_DRIVER_LAYER_PROJ_NAME                  "STM32F7_Driver_Layer-Stubs")
endif()

set(STM32F7_DRIVER_LAYER_GIT_PROJ_NAME              "stm32f7_driver_layer")
set(STM32F7_DRIVER_LAYER_GIT_REPOSITORY             "https://gitlab.com/stm32-driver-layer/stm32f7_driver_layer.git")
set(STM32F7_DRIVER_LAYER_GIT_TAG                    "v1.0.0")

set(STM32F7_DRIVER_LAYER_PROJECT_ROOT_FOLDER        "${PROJECT_ROOT_FOLDER}/build/${CMAKE_BUILD_TYPE}/${STM32F7_DRIVER_LAYER_GIT_PROJ_NAME}-src/")

if(DEFINED STM32F7_DRIVER_LAYER_PROJECT_LIB_FOLDER)
elseif((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_DEBUG_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_RELEASE_BUILD_NAME}))
    set(STM32F7_DRIVER_LAYER_PROJECT_LIB_FOLDER        "${PROJECT_ROOT_FOLDER}/build/${CMAKE_BUILD_TYPE}/${STM32F7_DRIVER_LAYER_GIT_PROJ_NAME}-src/build/${CMAKE_BUILD_TYPE}/Source/Production/")
elseif((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_TESTS_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_STUBS_BUILD_NAME}))
    set(STM32F7_DRIVER_LAYER_PROJECT_LIB_FOLDER        "${PROJECT_ROOT_FOLDER}/build/${CMAKE_BUILD_TYPE}/${STM32F7_DRIVER_LAYER_GIT_PROJ_NAME}-src/build/${PROJECT_STUBS_BUILD_NAME}/Source/Testing/Stubs/")
endif()

if(NOT DEFINED STM32F7_DRIVER_LAYER_PROJECT_INCLUDE_FOLDER)
    set(STM32F7_DRIVER_LAYER_PROJECT_INCLUDE_FOLDER    "${PROJECT_ROOT_FOLDER}/build/${CMAKE_BUILD_TYPE}/${STM32F7_DRIVER_LAYER_GIT_PROJ_NAME}-src/Source/")
endif()

include(DownloadProject)

function(check_stm32f7_driver_layer_dependency)
    coloredMessage(BoldMagenta "Well, it seems I need STM32F7_Driver_Layer Library. Let's see if I can find it!" STATUS)

    find_library(STM32F7_DRIVER_LAYER_LIB
                 NAMES      ${STM32F7_DRIVER_LAYER_PROJ_NAME}
                 HINTS      ${STM32F7_DRIVER_LAYER_PROJECT_LIB_FOLDER}
                 PATHS      ${STM32F7_DRIVER_LAYER_PROJECT_LIB_FOLDER})

    if(STM32F7_DRIVER_LAYER_LIB)
        coloredMessage(BoldMagenta "We have the libraries, let me just include the directory and link them to you (:" STATUS)
    else()
        coloredMessage(BoldMagenta "I couldn't find them, let me download and compile it for you (:" STATUS)

        #First download the project
        download_project(PROJ                ${STM32F7_DRIVER_LAYER_GIT_PROJ_NAME}
                         GIT_REPOSITORY      ${STM32F7_DRIVER_LAYER_GIT_REPOSITORY}
                         GIT_TAG             ${STM32F7_DRIVER_LAYER_GIT_TAG}
                         ${UPDATE_DISCONNECTED_IF_AVAILABLE}
                         QUIET)

        set(cmd_gradle  "gradle")

        if((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_DEBUG_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_RELEASE_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_STUBS_BUILD_NAME}))
            set(arg         "build${CMAKE_BUILD_TYPE}")
        elseif(${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_TESTS_BUILD_NAME})
            set(arg         "build${PROJECT_STUBS_BUILD_NAME}")
        endif()

        set(external_args   "-Pexternal_defines='${COMPILER_PROJECT_CONFIGS}'")

        if((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_DEBUG_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_RELEASE_BUILD_NAME}))
            #Second, compile it
            execute_process(COMMAND             ${cmd_gradle} 
                                                ${external_args} 
                                                ${arg}
                            WORKING_DIRECTORY   ${STM32F7_DRIVER_LAYER_PROJECT_ROOT_FOLDER}
                            OUTPUT_QUIET)
        elseif((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_TESTS_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_STUBS_BUILD_NAME}))
            set(gtest_configs_proj_name  "-Pgtest_configs_proj_name='${GTEST_PROJ_NAME}'")
            set(gtest_configs_lib_folder "-Pgtest_configs_lib_folder='${GTEST_PROJECT_LIB_FOLDER}'")
            set(gtest_configs_inc_folder "-Pgtest_configs_inc_folder='${GTEST_PROJECT_INCLUDE_FOLDER}'")
            set(gmock_configs_inc_folder "-Pgmock_configs_inc_folder='${GMOCK_PROJECT_INCLUDE_FOLDER}'")

            #Second, compile it
            execute_process(COMMAND             ${cmd_gradle} 
                                                ${external_args} 
                                                ${gtest_configs_proj_name} 
                                                ${gtest_configs_lib_folder} 
                                                ${gtest_configs_inc_folder} 
                                                ${gmock_configs_inc_folder} 
                                                ${arg}
                            WORKING_DIRECTORY   ${STM32F7_DRIVER_LAYER_PROJECT_ROOT_FOLDER}
                            OUTPUT_QUIET)
        endif()
    endif()

    link_directories(${STM32F7_DRIVER_LAYER_PROJECT_LIB_FOLDER})

    include_directories(${STM32F7_DRIVER_LAYER_PROJECT_INCLUDE_FOLDER})
endfunction()

function(link_stm32f7_driver_layer_libraries targetName)
    target_link_libraries(${targetName}
        lib${STM32F7_DRIVER_LAYER_PROJ_NAME}.a
    )
endfunction()
