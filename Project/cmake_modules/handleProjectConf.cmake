#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_RTOS_Library                                              #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

include(ProjectConf)

###############################
### GENERAL CONFIGURATIONS  ###
###############################

if(USE_COTIRE)
    # We use cotire, simply include it
    coloredMessage(BoldBlue "using cotire" STATUS)
    include(cotire)
else()
    # We do not use cotire, create dummy function.
    coloredMessage(BoldBlue "not using cotire" STATUS)
    function(cotire)
    endfunction(cotire)
endif()

if(USE_BOOST)
    # We use Boost, simply find and include it
    set(Boost_USE_STATIC_LIBS OFF)
    set(Boost_USE_MULTITHREADED ON)
    set(Boost_USE_STATIC_RUNTIME OFF)
    find_package(Boost 1.45.0 REQUIRED)

    if(Boost_FOUND)
        include_directories(${Boost_INCLUDE_DIRS})
        add_executable(progname file1.cxx file2.cxx)
        target_link_libraries(progname ${Boost_LIBRARIES})
    endif()
endif()

if(${STATICLIB_SWITCH} STREQUAL "ON")
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static -Wl,--whole-archive -lpthread -Wl,--no-whole-archive")
  set(CMAKE_FIND_LIBRARY_SUFFIXES ".a;.so")
endif()

#########################################
### GENERAL MIDDLEWARE CONFIGURATIONS ###
#########################################

string(FIND "${EXTERNAL_DEFINES}" "MIDDLEWARE_USE_STM32F7" retVal1)
if(${retVal1} EQUAL -1)
    set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -D${DEFAULT_MIDDLEWARE}")
endif()

###############################
###  BOARD CONFIGURATIONS   ###
###############################

string(FIND "${EXTERNAL_DEFINES}" "USE_ADAFRUIT_SHIELD" retVal1)
string(FIND "${EXTERNAL_DEFINES}" "USE_STM32F7xx_NUCLEO_144" retVal2)
string(FIND "${EXTERNAL_DEFINES}" "USE_STM32F769I_DISCO" retVal3)
string(FIND "${EXTERNAL_DEFINES}" "USE_STM32F769I_EVAL" retVal4)

if((${retVal1} EQUAL -1) AND (${retVal2} EQUAL -1) AND (${retVal3} EQUAL -1) AND (${retVal4} EQUAL -1))
    set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -D${DEFAULT_Board_Platform}")
endif()

###########################################
###          CMSIS CONFIGURATIONS       ###
###########################################

string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM7" retVal1)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM4" retVal2)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM3" retVal3)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0" retVal4)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0PLUS" retVal5)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_ARMV8MBL" retVal6)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_ARMV8MML" retVal7)

if((${retVal1} EQUAL -1) AND (${retVal2} EQUAL -1) AND (${retVal3} EQUAL -1) AND (${retVal4} EQUAL -1) AND
   (${retVal5} EQUAL -1) AND (${retVal6} EQUAL -1) AND (${retVal7} EQUAL -1))
    set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -D${DEFAULT_Building_Library}")
endif()

string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM7" retVal1)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM4" retVal2)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM3" retVal3)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0" retVal4)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0PLUS" retVal5)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_ARMV8MBL" retVal6)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_ARMV8MML" retVal7)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM7" retVal8)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM4" retVal9)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM3" retVal10)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0" retVal11)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0PLUS" retVal12)

if((${retVal1} EQUAL -1) AND (${retVal2} EQUAL -1) AND (${retVal3} EQUAL -1) AND (${retVal4} EQUAL -1) AND
   (${retVal5} EQUAL -1) AND (${retVal6} EQUAL -1) AND (${retVal7} EQUAL -1) AND (${retVal8} EQUAL -1) AND
   (${retVal9} EQUAL -1) AND (${retVal10} EQUAL -1) AND (${retVal11} EQUAL -1) AND (${retVal12} EQUAL -1))
    set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -D${DEFAULT_Processor_Series}")
endif()

#############################################
###          HAL CONFIGURATIONS           ###
#############################################

string(FIND "${EXTERNAL_DEFINES}" "USE_HAL_DRIVER" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_USE_HAL_DRIVER} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DUSE_HAL_DRIVER=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DUSE_HAL_DRIVER=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "USE_FULL_LL_DRIVER" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_USE_FULL_LL_DRIVER} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DUSE_FULL_LL_DRIVER=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DUSE_FULL_LL_DRIVER=0")
    endif()
endif()

####################################
### GENERAL RTOS CONFIGURATIONS  ###
####################################

string(FIND "${EXTERNAL_DEFINES}" "USE_NO_RTOS" retVal1)
string(FIND "${EXTERNAL_DEFINES}" "USE_FREERTOS" retVal2)

if((${retVal1} EQUAL -1) AND (${retVal2} EQUAL -1))
    set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -D${DEFAULT_RTOS}")
endif()
