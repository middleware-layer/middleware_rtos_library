#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_RTOS_Library                                              #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

###############################
### GENERAL CONFIGURATIONS  ###
###############################

option(USE_COTIRE                   "Use the COmpilation TIme REducer."                     ON)
option(USE_BOOST                    "Use the Boost Library."                                OFF)
option(STATICLIB_SWITCH             "Compile a statically linked version of the library."   OFF)

#########################################
### GENERAL MIDDLEWARE CONFIGURATIONS ###
#########################################

set(DEFAULT_MIDDLEWARE                  "MIDDLEWARE_USE_STM32F7")

###############################
###  BOARD CONFIGURATIONS   ###
###############################

#set(DEFAULT_Board_Platform              USE_ADAFRUIT_SHIELD)
#set(DEFAULT_Board_Platform              USE_STM32F7xx_NUCLEO_144)
set(DEFAULT_Board_Platform              "USE_STM32F769I_DISCO")
#set(DEFAULT_Board_Platform              USE_STM32F769I_EVAL)

###########################################
###          CMSIS CONFIGURATIONS       ###
###########################################

set(DEFAULT_Building_Library            "ARM_MATH_CM7")
#set(DEFAULT_Building_Library            ARM_MATH_CM4)
#set(DEFAULT_Building_Library            ARM_MATH_CM3)
#set(DEFAULT_Building_Library            ARM_MATH_CM0)
#set(DEFAULT_Building_Library            ARM_MATH_CM0PLUS)
#set(DEFAULT_Building_Library            ARM_MATH_ARMV8MBL)
#set(DEFAULT_Building_Library            ARM_MATH_ARMV8MML)

#set(DEFAULT_Processor_Series            STM32F722xx)
#set(DEFAULT_Processor_Series            STM32F723xx)
#set(DEFAULT_Processor_Series            STM32F732xx)
#set(DEFAULT_Processor_Series            STM32F733xx)
#set(DEFAULT_Processor_Series            STM32F745xx)
#set(DEFAULT_Processor_Series            STM32F746xx)
#set(DEFAULT_Processor_Series            STM32F756xx)
#set(DEFAULT_Processor_Series            STM32F765xx)
#set(DEFAULT_Processor_Series            STM32F767xx)
set(DEFAULT_Processor_Series            "STM32F769xx")
#set(DEFAULT_Processor_Series            STM32F777xx)
#set(DEFAULT_Processor_Series            STM32F779xx)

#############################################
###          HAL CONFIGURATIONS           ###
#############################################

set(DEFAULT_USE_HAL_DRIVER              ON)
set(DEFAULT_USE_FULL_LL_DRIVER          OFF)

####################################
### GENERAL RTOS CONFIGURATIONS  ###
####################################

#set(DEFAULT_RTOS              USE_NO_RTOS)
set(DEFAULT_RTOS              USE_FREERTOS)
